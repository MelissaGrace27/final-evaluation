class Pet(): #Original Class
    def __init__(self, species, name = "Kitty"):
        try:
            self.species = species
            self.name = name
        except:
            for i in ['dog', 'cat' , 'horse', 'hamster']:
                if species != i:
                    print("error")

    def __str__(self):
        if(self.name):
            state = "Species of:" + self.species + ", "+ "named" + self.name
        else:
            state = "Species of:" + self.species + ", "+ "unnamed"
        return state

class Dog(Pet):
    def __init__(self, name, chases='Cats'):
        Pet.__init__(self, species="Dog")  # call the parent class's constructor
        self.name = name
        self.chases = chases #assigns the new instance variable

    def __str__(self):
        if(self.name):
            state = "Species of:" + self.species + ", " + "named " + self.name + ", chases " + self.chases
        else:
            state = "Species of:" + self.species + ", " + "unnamed, chases  " + self.chases
        return state


class Cat(Pet):
    def __init__(self, name, hates='Dogs'):
        Pet.__init__(self, species="Cat")  # call the parent class's constructor
        self.name = name
        self.hates = hates  # assigns the new instance variable

    def __str__(self):
        if (self.name):
            state = "Species of:" + self.species + ", " + "named " + self.name + ", hates " + self.hates
        else:
            state = "Species of:" + self.species + ", " + "unnamed, hates  " + self.hates
        return state

def whichone(petlist, name):
    for pet in petlist:
        if pet.name == name:
            return pet
    return None # no pet matched

pet_types = {'dog': Dog, 'cat': Cat}
def whichtype(adopt_type="general pet"):
    return pet_types.get(adopt_type.lower(), Pet)


def main():
    animals = []
    prompt = """
           Pet <petname with no spaces> <pet_type - choose dog, cat>
           Quit

           Choice: """
    ch = ""
    while True:
        action = input(ch + "\n" + prompt)
        ch = ""
        words = action.split()
        if len(words) > 0:
            command = words[0]
        else:
            command = None
        if command == "Quit":
            print("Exiting...")
            return
        elif command == "Pet" and len(words) > 1:
            if whichone(animals, words[1]):
                ch += "You already have a pet with that name\n"
            else:
                # figure out which class it should be
                if len(words) > 2:
                    Cl = whichtype(words[2])
                else:
                    Cl = Pet
                # Make an instance of that class and append it
                animals.append(Cl(words[1]))
        else:
            ch += "I didn't understand that. Please try again."

        for pet in animals:
            ch += "\n" + pet.__str__()

main()